from time import sleep


def main():
    for i in range(3):
        print(f'tick {i}')
        sleep(.5)


if __name__ == '__main__':
    main()
