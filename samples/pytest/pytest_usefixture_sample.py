import pytest

file = 'foobar.txt'


@pytest.fixture
def prepare_file():
    with open(file, 'w') as f:
        f.write('hello')


@pytest.mark.usefixtures("prepare_file")
def test_hello():
    with open(file, 'r') as f:
        content = f.read()
    assert content == 'hello'
