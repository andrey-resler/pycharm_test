class Foo:
    def __init__(self, x: int, y: str):
        self.x = x
        self.y = y

    def do(self):
        print(f'{self.x} and {self.y}')


class Bar(Foo):
    def __init__(self, x: int, y: str):
        super().__init__(x, y)


a = Foo(10, '20')
a.do()

b = Bar(10, '20')
b.do()
