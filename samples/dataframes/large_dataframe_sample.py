import random
import datetime
import pandas as pd

first_names = ['Alice', 'Bob', 'Charlie', 'David', 'Emily', 'Frank', 'Grace', 'Henry', 'Isabel', 'John', 'Katie',
               'Liam', 'Maggie', 'Nathan', 'Olivia', 'Paul', 'Quinn', 'Rachel', 'Samuel', 'Tina', 'Ursula', 'Victor',
               'Wendy', 'Xander', 'Yvonne', 'Zachary']
last_names = ['Adams', 'Baker', 'Chen', 'Davis', 'Evans', 'Ford', 'Garcia', 'Huang', 'Irwin', 'Jones', 'Kim', 'Lee',
              'Miller', 'Nguyen', 'Olsen', 'Patel', 'Quinn', 'Ramirez', 'Singh', 'Taylor', 'Upton', 'Vargas', 'Wu',
              'Xu', 'Yoon', 'Zhang']

l = []
count = 10000000

for i in range(count):
    first_name = random.choice(first_names)
    last_name = random.choice(last_names)
    birthdate = datetime.date(random.randint(1950, 2022), random.randint(1, 12), random.randint(1, 28))
    l.append([first_name, last_name, birthdate])

df = pd.DataFrame(l)
