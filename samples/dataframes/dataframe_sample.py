import pandas as pd
import numpy as np

# arr = np.array([[1,2,3],[4,5,6]])

rows = 100
columns = 100
col_names = [f'some_long_name_{i}' for i in range(columns)]
df = pd.DataFrame(np.random.rand(rows, columns), columns=col_names)
print(df)
