import argparse


def main():
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--hash-file", type=str, help="User hash file")
    parser.add_argument("-a", "--action", type=str, help="Choose action: import or export")
    parser.add_argument("-f", "--file", type=str, help="Source file for import")
    args = parser.parse_args()

    if args.hash_file is None:
        print('\nWARNING: User hash file not specified\n')
        parser.print_help()
        exit()

    if args.action is None:
        print('\nWARNING: Action not specified\n')
        parser.print_help()
        exit()

    main()
