import logging
from time import sleep

logging.basicConfig(
    filename='test.log',
    filemode='w',
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s: %(message)s',
)

for i in range(100):
    if i % 2 == 0:
        logging.info('Info test')
    else:
        logging.warning('Warning test')
    sleep(.5)

# logging.error('Error test')
# logging.debug('Debug test')
