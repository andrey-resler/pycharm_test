from setuptools import setup
from Cython.Build import cythonize

# Install with `python setup.py build_ext --inplace`
setup(
    ext_modules=cythonize("fib.pyx"),
)
